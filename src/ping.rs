// SPDX-FileCopyrightText: 2023 Kamila Borowska <kamila@borowska.pw>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::Context;
use anyhow::Result;
use poise::{command, CreateReply};
use std::time::Instant;

/// Calculates the time it takes for a message sent by xbot to be processed by Discord.
#[command(prefix_command, hide_in_help)]
pub async fn ping(ctx: Context<'_>) -> Result<()> {
    let now = Instant::now();
    ctx.say("Pong!")
        .await?
        .edit(
            ctx,
            CreateReply::default().content(format!("Pong! Took {:?}.", now.elapsed())),
        )
        .await?;
    Ok(())
}
