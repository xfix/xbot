// SPDX-FileCopyrightText: 2023 Kamila Borowska <kamila@borowska.pw>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use crate::Context;
use anyhow::Result;
use poise::command;

/// Get the link to the source code for this bot.
#[command(prefix_command, slash_command)]
pub async fn source(ctx: Context<'_>) -> Result<()> {
    ctx.say("<https://codeberg.org/xfix/xbot>").await?;
    Ok(())
}
